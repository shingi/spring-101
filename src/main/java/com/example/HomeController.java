package com.example;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	private final Knight knight;
	
	public HomeController(Knight knight) {
		this.knight = knight;
	}
	
	@RequestMapping("/")
	public String hello() {
		knight.embarkOnQuest();
		return "Hello, World!";
	}
}
