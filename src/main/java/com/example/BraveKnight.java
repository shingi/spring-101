package com.example;

public class BraveKnight implements Knight {

	private final Quest quest;
	
	public BraveKnight(Quest quest) {
		this.quest = quest;
	}

	@Override
	public void embarkOnQuest() {
		this.quest.embark();
		
	}
}
