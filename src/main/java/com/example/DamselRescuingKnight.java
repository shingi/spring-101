package com.example;

public class DamselRescuingKnight implements Knight {

	private final RescueDamselQuest quest;
	
	public DamselRescuingKnight() {
		// COUPLING HERE
		this.quest = new RescueDamselQuest();
	}
	
	public void embarkOnQuest() {
		quest.embark();
	}

}
